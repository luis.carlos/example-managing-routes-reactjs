import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Dashboard from '../page/dashboard';
import Register from '../page/register';
import Login from '../page/login';
import VacanciesDetail from '../components/VacanciesDetail/VacanciesDetail';
import { AuthProvider } from './auth';
import RequireAuth from './PrivateRoute';
import VacanciesList from '../components/VacanciesList/VacanciesList';

const RoutesPage = () => {
    return (
        <Router>
            <AuthProvider>
                <Routes>
                    <Route exact path='/' element={<Login />} />
                    <Route exact path='/register' element={<Register />} />
                    <Route path='*' component={() => <h1>Page not found</h1>} />
                    <Route
                        path='/dashboard'
                        element={
                            <RequireAuth>
                                <Dashboard />
                            </RequireAuth>
                        }
                    />
                    <Route
                        path='/vacancies'
                        element={
                            <RequireAuth>
                                <VacanciesList />
                            </RequireAuth>
                        }
                    />
                    <Route
                        path='/vacancies/:id'
                        element={
                            <RequireAuth>
                                <VacanciesDetail />
                            </RequireAuth>
                        }
                    />
                </Routes>
            </AuthProvider>
        </Router>
    );
};

export default RoutesPage;
