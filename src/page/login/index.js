import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { auth, logInWithEmailAndPassword } from '../../services/firebase';
import { useAuthState } from 'react-firebase-hooks/auth';
import './styles.css';

function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [user, loading, error] = useAuthState(auth);
    const navigate = useNavigate();

    useEffect(() => {
        if (loading) {
            // maybe trigger a loading screen
            return;
        }
        if (user) navigate('/dashboard');
    }, [user, loading]);

    return (
        <div className='login'>
            <div className='login__container'>
                <input
                    type='text'
                    className='login__textBox'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder='E-mail'
                />
                <input
                    type='password'
                    className='login__textBox'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder='Senha'
                />
                <button
                    className='login__btn'
                    onClick={() => logInWithEmailAndPassword(email, password)}
                >
                    Entrar
                </button>
                <div>
                    Não tem uma conta? <Link to='/register'>Cadastre-se</Link>
                </div>
            </div>
        </div>
    );
}

export default Login;
