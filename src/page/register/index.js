import React, { useEffect, useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { Link, useNavigate } from 'react-router-dom';
import { auth, registerWithEmailAndPassword } from '../../services/firebase';

import './styles.css';

function Register() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [user, loading, error] = useAuthState(auth);
    const navigate = useNavigate();

    const register = () => {
        if (!name) alert('Please enter name');
        registerWithEmailAndPassword(name, email, password);
    };

    useEffect(() => {
        if (loading) return;
        if (user) navigate('/dashboard');
    }, [user, loading]);

    return (
        <div className='register'>
            <div className='register__container'>
                <input
                    type='text'
                    className='register__textBox'
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    placeholder='Nome'
                />
                <input
                    type='text'
                    className='register__textBox'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder='E-mail'
                />
                <input
                    type='password'
                    className='register__textBox'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder='Senha'
                />
                <button className='register__btn' onClick={register}>
                    Cadastra
                </button>

                <div>
                    Tem uma conta? <Link to='/'>Entrar</Link>
                </div>
            </div>
        </div>
    );
}

export default Register;
