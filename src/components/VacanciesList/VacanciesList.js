import { Link } from 'react-router-dom';

const jobs = [
    {
        id: 1,
        title: 'Desenvolvimento Back-End',
        job: 'ANALISTA DE DESENVOLVIMENTO JAVA WEB',
        experience: [
            {
                id: 1,
                name: 'Será repleto de desafios, estará em contato com cliente internacional, grande potência na área de manufatura e com um trabalho de impacto mundial. Em um ambiente descontraído e uma equipe colaborativa de alta performance;',
            },
            {
                id: 2,
                name: 'Suas responsabilidades serão voltadas para o desenvolvimento de aplicações web;',
            },
            {
                id: 3,
                name: 'Projeto utilizando metodologia ágil.',
            },
        ],
        differentials: [
            {
                id: 1,
                name: 'Experiência em desenvolvimento Java e de componentes utilizando Spring (Data, Security, Boot), JSF (Primefaces/ IceFaces, etc) e Web services RESTful;',
            },
            {
                id: 2,
                name: 'Experiência em mapeamento objeto relacional com JPA (Hibernate) e banco de dados (MySQL);',
            },
            {
                id: 3,
                name: 'Experiência com gerenciamento de dependências (Maven);',
            },
        ],
    },
    {
        id: 2,
        title: 'Desenvolvimento Back-End',
        job: 'ANALISTA DE DESENVOLVIMENTO NODE.JS PL',
        experience: [
            {
                id: 1,
                name: 'Experiência em programação Node.js;',
            },
            {
                id: 2,
                name: 'Experiência em Typescript;',
            },
            {
                id: 3,
                name: 'Experiência e/ou conhecimento em banco de dados relacionais e/ou não relacionais;',
            },
            {
                id: 4,
                name: 'Experiência em desenvolvimento de REST APIs;',
            },
            {
                id: 5,
                name: 'Experiência com Git (gitflow);',
            },
            {
                id: 6,
                name: 'Experiência em testes unitários/integração (Jest / Mocha / Supertest);',
            },
        ],
        differentials: [
            {
                id: 1,
                name: 'Experiência em arquitetura de micro serviços;',
            },
            {
                id: 2,
                name: 'Conhecimento em contêineres com Docker;',
            },
            {
                id: 1,
                name: 'Conhecimento de lib/frameworks ReactJS, Angular ou Vue;',
            },
        ],
    },
];

function VacanciesList() {
    return (
        <ul>
            {jobs.map((job) => {
                return (
                    <li key={job.id}>
                        <Link to={`/vacancies/${job.id}`}>
                            {job.title} - {job.job}
                        </Link>
                    </li>
                );
            })}
        </ul>
    );
}

export default VacanciesList;
