import { Link, useParams } from 'react-router-dom';
import '../../styles/index.css';

function VacanciesDetail() {
    let params = useParams();
    if (!params.id) return null;

    return (
        <div id='container'>
            <h2>Detalhes</h2>
            <br />
            <h2>{params.id}</h2>
            <Link to='/'>Voltar </Link>
        </div>
    );
}

export default VacanciesDetail;
